package p1c1.practica1;

import java.util.Scanner;

public class Practica1 {

    public static void main(String[] args) {
        System.out.println("Practica de Productos");
        
         Scanner cap = new Scanner(System.in);
         Productos pro=new Productos();
         
         System.out.println("Codigo del Producto: ");
         pro.codPro=cap.nextLine();
         
         System.out.println("Descripcion: ");
         pro.des=cap.nextLine();
         
         System.out.println("Unidad De medida: ");
         pro.unidad=cap.nextLine();
         
         System.out.println("Precio De Compra: ");
         pro.precioCom=cap.nextFloat();
         
         System.out.println("Precio De Venta: ");
         pro.precioVen=cap.nextFloat();
         
         System.out.println("Cantidad: ");
         pro.cant=cap.nextInt();
         
         pro.mostrar();
        
    }
}
