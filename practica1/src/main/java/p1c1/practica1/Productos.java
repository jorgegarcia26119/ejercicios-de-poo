
package p1c1.practica1;


public class Productos {
    
    protected String codPro,des,unidad;
    protected float precioVen, precioCom, cant;

    //Constructor
    public Productos(){
        codPro="";
        des="";
        unidad="";
        precioVen=0.02f;
        precioCom=0.02f;
        cant=0.02f;       
    }
    //Constructor 2
    public Productos(String codPro, String des, String unidad, float precioVen, float precioCom, float cant){    
        this.codPro=codPro;
        this.des=des;
        this.unidad=unidad;
        this.precioVen=precioVen;
        this.precioCom=precioCom;
        this.cant=cant;
    }
    //Constructor 3
    public Productos(Productos pro){ 
        this.codPro=codPro;
        this.des=des;
        this.unidad=unidad;
        this.precioVen=precioVen;
        this.precioCom=precioCom;
        this.cant=cant;
    }
    
    public void setCod(String cod){
        codPro=cod;
    }
    
    public String getCod(){
        return codPro;
    }
    
    public void setDes(String desc){
        des=desc;
    }
    
    public String getDes(){
        return des;
    }
    
    public void setUni(String uni){
        unidad=uni;
    }
    
    public String getUni(){
        return unidad;
    }
    
    public void setPreVe(float preV){
        precioVen=preV;
    }
    
    public float getPreV(){
        return precioVen;
    }
    
    public void setPreCom(float preC){
        precioCom=preC;
    }
    
    public float getPreC(){
        return precioCom;
    }
    
    public void setCant(float can){
        cant=can;
    }
    
    public float getCant(){
        return cant;
    }
    
    public float calcularPrecVen(){
        return getPreV()*getCant();
    }
    
    public float calcularPrecCom(){
        return getPreC()*getCant();
    }
    
    public float calcularGanancia(){
        return calcularPrecVen()-calcularPrecCom();
    }
   
    public void mostrar(){
        
        System.out.println("Codigo Producto: "+  getCod());
        System.out.println("Descripcion: "+ getDes());
        System.out.println("Unidad de Medida: "+ getUni());
        System.out.println("Precio Venta: "+ getPreV());
        System.out.println("Precio Compra: "+ getPreC());
        System.out.println("Cantidad; "+ getCant());
        System.out.println("Calculo precio venta: "+ calcularPrecVen());
        System.out.println("Calculo precio compra: "+ calcularPrecCom());
        System.out.println("Calculo de ganancia: "+ calcularGanancia());
    }
}

